using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathHelper
{
    const int mille = 1000; //K
    public const float milione = 1000000; //M
    public const float miliardo = 1000000000; //G
    public const float bilione = 1000000000000; //T
    public const float biliardo = 1000000000000000; //P
    public const float trilione = 1000000000000000000; //E


    static public string FormatOverMillion(float val,ref float floatVal)
    {
        if (val >= milione)
        {
            if (val / miliardo >= 1)
            {
                if (val / bilione >= 1)
                {
                    if (val / biliardo >= 1)
                    {
                        if (val / trilione >= 1)
                        {
                            floatVal = val/(float)trilione;

                            return "E";
                        }
                        floatVal = val /(float)biliardo;
                        return "P";
                    }
                    floatVal = val / (float)bilione;
                    return "T";
                }
                floatVal = val / (float)miliardo;
                return "G";
            }
            floatVal = val / (float)milione;
            return "M";
        }
        else return "";
    }

    static public string FormatOverthousands(float val,ref float refVal)
    {
        if (val >= milione) return FormatOverMillion(val,ref refVal);
        else
        {
            if (val >= mille)
            {
                refVal = val / (float)mille;
                return "K";
            }
            else
            {
                refVal = val;
                return "";
            }
        }

    }
}
