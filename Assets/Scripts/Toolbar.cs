using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toolbar : MonoBehaviour
{
    public Animator FarmAnim, ShopAnim;
    public Button FarmButton;
    public Bonus GPU_FarmBonus;
    // Start is called before the first frame update
    private void OnEnable()
    {
        if(CheckIfFarmIsUnlocked())
        {
            FarmButton.interactable = true;
        }
        else
        {
            EventManager.EV_ResourcesUpdate += UnlockFarm;
            EventManager.EV_ResetAll += LockFarm;
        }
    }

    private void LockFarm(bool levelUp)
    {
        FarmButton.interactable = false;
        EventManager.EV_ResourcesUpdate += UnlockFarm;
    }

    void UnlockFarm(Resources res)
    {
        if (CheckIfFarmIsUnlocked())
        {
            FarmButton.interactable = true;
            EventManager.EV_ResourcesUpdate -= UnlockFarm;
        }
    }
    bool CheckIfFarmIsUnlocked()
    {
        return GPU_FarmBonus.isUnlocked;
    }
    public void OpenShop()
    {
        CloseFarm();
        ShopAnim.SetBool("Active", true);
    }
    public void OpenFarm()
    {
        CloseShop();
        FarmAnim.SetBool("Active", true);
    }
    public void CloseAll()
    {
        CloseShop();
        CloseFarm();
    }

    void CloseShop() => ShopAnim.SetBool("Active", false);
    void CloseFarm() => FarmAnim.SetBool("Active", false);
}
