using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMiner : MonoBehaviour
{
    public GPU_Farm Farm;
    Resources resources;
    // Start is called before the first frame update
    void Start()
    {

        resources = GameManager.instance.resources;
        StartAutomining();
        StartAutoSave();
    }
    public void StartAutoSave()
    {
        CancelInvoke("AutoSave");
        InvokeRepeating("AutoSave",0,1.5f);
    }
    public void StartAutomining()
    {
        CancelInvoke("PickMoney");
        InvokeRepeating("PickMoney",0, 1);
    }

    public void StopAutoMining()
    {
        CancelInvoke();
    }
    void AutoSave()
    {
        EventManager.SaveData();
    }
    void PickMoney()
    {
        resources.PickMoneyFromBonus();
        Farm.PickMoneyFromAllGPU();
        //EventManager.SaveData();
    }
}
