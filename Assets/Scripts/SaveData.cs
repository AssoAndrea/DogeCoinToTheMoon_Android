using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public float Money;
    public int farmLevel,gameLevel;
    public List<GPU_Info> GPU_data = new List<GPU_Info>();
    public List<Bonus_Info> Bonus_data = new List<Bonus_Info>();

    public float MoneyMultiplier;

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }
    public void LoadFromJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
    }
}

[System.Serializable]
public struct GPU_Info
{
    public int ID;
    public bool locked, active;
    public float duration;
    public float efficency;
}

[System.Serializable]
public struct Bonus_Info
{
    public int ID;
    public uint level;
    public bool isUnlocked;
}

public interface ISavable
{
    void PopulateSaveData(ref SaveData saveData);
    void LoadFromSaveData(SaveData saveData);
}
