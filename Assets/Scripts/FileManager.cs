using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileManager
{
    public static bool LoadFromFile(string fileName,out string result)
    {
        string fullPath = Path.Combine(Application.persistentDataPath, fileName);
        try
        {
            result = File.ReadAllText(fullPath);
            return true;
        }
        catch (System.Exception e)
        {
            result = "";
            return false;
        }
    }
    public static bool WriteToFile(string fileName,string fileContent)
    {
        string fullPath = Path.Combine(Application.persistentDataPath, fileName);
        try
        {
            File.WriteAllText(fullPath, fileContent);
            //Debug.Log(fullPath);
            return true;
        }
        catch (System.Exception e)
        {
            //Debug.LogWarning($"fail to write to {fullPath} with error {e.Message}");
            return false;
        }
    }
}
