using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD_Spawner : MonoBehaviour
{
    public TMPro.TMP_Text textToSpawn;
    public RectTransform[] positionsToSpawn;
    public void Spawn(float money)
    {
        string textToDisplay;
        if (money > MathHelper.milione)
        {
            float formattedMoney = money;
            string format = MathHelper.FormatOverMillion(money,ref formattedMoney);
            textToDisplay = formattedMoney.ToString("#.#")+format;
        }
        else
        {
            textToDisplay = money.ToString("#,#");
        }
        int index = Random.Range(0, positionsToSpawn.Length);
        TMPro.TMP_Text spawnedText = Instantiate(textToSpawn,positionsToSpawn[index].transform);
        spawnedText.text = textToDisplay;
    }
}
