using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;


public class ADS_BannerMgr : MonoBehaviour
{

    private string gameId = "4184801";
    string ID = "banner";
    bool testMode = false;
    // Start is called before the first frame update
    void Start()
    {
        Advertisement.Initialize(gameId,testMode);
        ShowBanner();
    }
    void ShowBanner()
    {
        if (Advertisement.IsReady(ID))
        {
            Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
            Advertisement.Banner.Show(ID);
        }
        else StartCoroutine(RepatShowBanner());
    }
    IEnumerator RepatShowBanner()
    {
        yield return new WaitForSeconds(1);
        ShowBanner();
    }
}
