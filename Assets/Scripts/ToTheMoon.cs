using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToTheMoon : MonoBehaviour
{
    public Button button;
    public MultiplierCost costs;
    public TMPro.TMP_Text costText;
    float buyCost;
    string originalString;

    private void OnEnable()
    {
        originalString = costText.text;
        EventManager.EV_ResourcesUpdate += UpdateText;
    }

    private void UpdateText(Resources res)
    {
        buyCost = costs.GetCost(res.GameLevel);
        if (res.Money > buyCost)
        {
            costText.gameObject.SetActive(false);
            button.interactable = true;
        }
        else
        {
            button.interactable = false;
            costText.gameObject.SetActive(true);
            float roundedCost=buyCost;
            string format = MathHelper.FormatOverthousands(buyCost, ref roundedCost);
            costText.text = originalString.Replace("{v}", roundedCost.ToString("#.#")+format);
        }
    }
    public void ResetWorld()
    {
        EventManager.ResetGame(levelUp: true);
        GameManager.instance.resources.UpdateMultiplier(costs);
    }
}
