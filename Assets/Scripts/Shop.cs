using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public BonusSlot[] slots;
    public GPU_Farm Gpu_farm;

    int dps;
    // Start is called before the first frame update
    void Start()
    {
        slots = FindObjectsOfType<BonusSlot>();
        dps = 0;

    }
    private void OnEnable()
    {
        EventManager.EV_ResourcesUpdate += UpdateDps;
        EventManager.EV_ResetAll += ResetAllBonus;

    }
    private void ResetAllBonus(bool levelUp)
    {
        foreach (BonusSlot item in slots)
        {
            item.bonus.Reset();
            item.UpdateSlot();
        }

    }

    void UpdateDps(Resources resources)
    {
        foreach (BonusSlot item in slots)
        {
            if (item.bonus.isUnlocked && item.bonus.isAutomaticDps)
            {
                dps += item.bonus.dps;
            }
        }
        resources.SetShopDps(dps);
        dps = 0;
    }
}
