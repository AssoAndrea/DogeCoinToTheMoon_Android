using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public delegate void OnResourcesUpdate(Resources res);
    public static OnResourcesUpdate EV_ResourcesUpdate;
    public static void ResourcesUpdated(Resources res)
    {
        EV_ResourcesUpdate(res);
    }

    public delegate void OnSaveData(ref SaveData saveData);
    public static OnSaveData EV_SaveData;
    public static void SaveData()
    {
        SaveData saveData = new SaveData();
        EV_SaveData?.Invoke(ref saveData);
        FileManager.WriteToFile(GameManager.instance.saveName, saveData.ToJson());
    }

    public delegate void OnLoadData(SaveData saveData);
    public static OnLoadData EV_LoadData;
    public static bool LoadData()
    {
        if (FileManager.LoadFromFile(GameManager.instance.saveName,out string json))
        {

            SaveData sd = new SaveData();
            sd.LoadFromJson(json);
            EV_LoadData?.Invoke(sd);
            return true;
        }
        else
        {
            return false;
        }
    }

    public delegate void OnResetAll(bool levelUp);
    public static OnResetAll EV_ResetAll;
    public static void ResetGame(bool levelUp = false)
    {
        EV_ResetAll?.Invoke(levelUp);
        EV_ResourcesUpdate?.Invoke(GameManager.instance.resources);
    }
}
