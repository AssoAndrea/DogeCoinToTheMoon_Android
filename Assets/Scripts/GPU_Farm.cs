using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPU_Farm : MonoBehaviour, ISavable
{
    Animator anim;
    Resources res;
    public Bonus Efficency, Duration;
    public MultiplierCost component;
    bool windowOpen = false;

    [TextArea(1, 3)]
    public string LockedText, ActiveText, BrokenText;

    [Space(20)]
    public int level;
    public float dps;
    public GPU_Slot[] Gpus;
    // Start is called before the first frame update
    void Start()
    {
        res = GameManager.instance.resources;
        anim = GetComponent<Animator>();
        anim.SetBool("Active", windowOpen);
        UpdateGpuSlotText();
        SetAllGPu();

    }
    public void SetAllGPu()
    {
        foreach (GPU_Slot item in Gpus)
        {
            item.LevelUpDuration(Duration);
            item.LevelUpEfficency(Efficency);
        }
    }
    private void OnEnable()
    {
        EventManager.EV_SaveData += PopulateSaveData;
        EventManager.EV_LoadData += LoadFromSaveData;
        EventManager.EV_ResetAll += ResetGPU;
    }

    private void ResetGPU(bool levelUp)
    {
        foreach (GPU_Slot item in Gpus)
        {
            item.ResetGPU();
            item.UpdateText(LockedText, ActiveText, BrokenText);
        }
    }

    public void OpenAndCloseWindow()
    {
        windowOpen = !windowOpen;
        anim.SetBool("Active", windowOpen);
    }

    public void LevelUpDuration(float val)
    {
        level++;
        foreach (GPU_Slot item in Gpus)
        {
            item.LevelUpDuration(Duration);
        }
        UpdateGpuSlotText();
    }
    public void LevelUpEfficency(float val)
    {
        level++;
        foreach (GPU_Slot item in Gpus)
        {
            item.LevelUpEfficency(Efficency);
        }
        UpdateGpuSlotText();
    }

    public void UpdateGpuSlotText()
    {
        dps = 0;
        foreach (GPU_Slot item in Gpus)
        {
            if (item.GPU.Active)
            {
                dps += item.PickMoney(false);
            }
            item.UpdateText(LockedText, ActiveText, BrokenText);
        }
        GameManager.instance.resources.SetFarmDps((int)dps);
    }

    public void PickMoneyFromAllGPU()
    {
        float moneyToAdd = 0;
        foreach (GPU_Slot gpu in Gpus)
        {
            if (gpu.Active())
            {
                moneyToAdd += gpu.PickMoney();
            }
        }
        res.AddOrRemoveMoney(moneyToAdd);
        UpdateGpuSlotText();
    }




    public void PopulateSaveData(ref SaveData saveData)
    {
        saveData.farmLevel = level;
        
        foreach (GPU_Slot item in Gpus)
        {
            GPU_Bonus gpu = item.GPU;
            GPU_Info info = new GPU_Info();
            info.ID = gpu.ID;
            info.active = gpu.Active;
            info.locked = gpu.Locked;
            info.efficency = gpu.efficencyMul;
            info.duration = gpu.CurrDuration;
            saveData.GPU_data.Add(info);
        }
    }

    public void LoadFromSaveData(SaveData saveData)
    {
        level = saveData.farmLevel;

        foreach (GPU_Slot item in Gpus)
        {
            GPU_Bonus gpu = item.GPU;
            foreach (GPU_Info info in saveData.GPU_data)
            {
                if (info.ID == gpu.ID)
                {
                    gpu.Load(info);
                }
            }
            item.UpdateText(LockedText, ActiveText, BrokenText);

        }
    }
}
