using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class ADS_Manager : MonoBehaviour, IUnityAdsListener
{

#if UNITY_IOS
    private string gameId = "4184800";
#elif UNITY_ANDROID
    private string gameId = "4184801";
#endif

    string RewardID = "rewardedVideo";
    string InterstitialID = "Interstitial";
    bool testMode = false;
    public GameObject RewardButton;
    public TMPro.TMP_Text RewardText;
    public float timerToDisplayNewAds;
    public float timerToDestroyAds = 7;
    public float MoneyMul= 2;
    public int TimeSkip = 30;

    float MoneyToReward;
    void Start()
    {
        // Initialize the Ads service:
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, testMode);
        RewardButton.SetActive(false);
        StartRewardTimer();

    }

    void StartRewardTimer()
    {
        StopAllCoroutines();
        StartCoroutine(AwaitForNewAds());
    }
    IEnumerator AwaitForNewAds()
    {
        yield return new WaitForSeconds(timerToDisplayNewAds);

        if (Advertisement.IsReady(RewardID))
        {
            ShowRewardButton();
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(AwaitForNewAds());
        }
    }

    void ShowRewardButton()
    {
        MoneyToReward = GameManager.GetADS_Bonus(TimeSkip,MoneyMul);
        if(MoneyToReward< 500)
        {
            MoneyToReward = 500;
        }
        RewardButton.SetActive(true);
        float valToDisplay = MoneyToReward;
        string format = "";
        if (MoneyToReward >= 1000)
        {
            format = MathHelper.FormatOverthousands(MoneyToReward, ref valToDisplay);
        }
        RewardText.text = "+" + valToDisplay.ToString("#.#")+format;

        //destroy ads after n seconds
        StopAllCoroutines();
        StartCoroutine(DestructionADS_Timer());
    }

    IEnumerator DestructionADS_Timer()
    {
        yield return new WaitForSeconds(timerToDestroyAds);
        StartRewardTimer();
        RewardButton.SetActive(false);

    }



    public void ShowInterstitialAd()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady())
        {
            Advertisement.Show(InterstitialID);
            // Replace mySurfacingId with the ID of the placements you wish to display as shown in your Unity Dashboard.
        }
        else
        {
            //Debug.Log("Interstitial ad not ready at the moment! Please try again later!");
        }
    }
    public void ShowRewardedVideo()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady(RewardID))
        {
            Advertisement.Show(RewardID);
            StopAllCoroutines();
        }
        else
        {
            //Debug.Log("Rewarded video is not ready at the moment! Please try again later!");
        }
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string surfacingId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            GameManager.instance.resources.AddOrRemoveMoney(MoneyToReward);
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            //Debug.LogWarning("The ad did not finish due to an error.");
        }
        StartRewardTimer();
        RewardButton.SetActive(false);
    }

    #region ADS Callback
    public void OnUnityAdsReady(string surfacingId)
    {
        // If the ready Ad Unit or legacy Placement is rewarded, show the ad:
        if (surfacingId == RewardID)
        {
            // Optional actions to take when theAd Unit or legacy Placement becomes ready (for example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string surfacingId)
    {
        // Optional actions to take when the end-users triggers an ad.
    } 
    #endregion

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }
}
