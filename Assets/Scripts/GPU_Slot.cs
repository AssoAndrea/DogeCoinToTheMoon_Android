using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class GPU_Slot : MonoBehaviour
{
    public GPU_Bonus GPU;
    public TMP_Text t_locked, t_active, t_broken;
    public GameObject LockedSlot, UnlockedSlot,BrokenSlot;
    public float FontSize;
    Resources res;



    private void Start()
    {
        res = GameManager.instance.resources;
        //GPU.slot = this;
        UpdateSlot();
        t_locked.fontSize = FontSize;
        t_active.fontSize = FontSize;
        t_broken.fontSize = FontSize;
    }


    public void UpdateSlot()
    {
        if (GPU.Locked)
        {
            LockedSlot.SetActive(true);
            UnlockedSlot.SetActive(false);
            BrokenSlot.SetActive(false);
        }
        else if (GPU.Active)
        {
            LockedSlot.SetActive(false);
            UnlockedSlot.SetActive(true);
            BrokenSlot.SetActive(false);
        }
        else
        {
            LockedSlot.SetActive(false);
            UnlockedSlot.SetActive(false);
            BrokenSlot.SetActive(true);
        }
    }

    public void Unlock()
    {
        if (res.Money > GPU.UnlockCost)
        {
            res.AddOrRemoveMoney(GPU.UnlockCost);

            LockedSlot.SetActive(false);
            UnlockedSlot.SetActive(true);
            GPU.Unlock();
            UpdateSlot();

        }
        else Debug.Log("Pochi soldi");
    }
    public void Buy()
    {
        if (res.Money > GPU.rebuyCost)
        {
            res.AddOrRemoveMoney(GPU.rebuyCost);
            GPU.Buy();
            UpdateSlot();

        }
    }
    public void ResetGPU()
    {
        GPU.Reset();
        UpdateSlot();
    }

    public void UpdateText(string locked,string active,string broken)
    {
        float rCost = 0;
        float rDps = 0;
        float rDur = 0;
        float rRebuy = 0;


        string costFormat = MathHelper.FormatOverthousands(GPU.UnlockCost,ref rCost);
        string dpsFormat = MathHelper.FormatOverthousands(GPU.GetCurrentDPS(),ref rDps);
        string durFormat = MathHelper.FormatOverthousands(GPU.CurrDuration,ref rDur);
        string rebuyFormat = MathHelper.FormatOverthousands(GPU.rebuyCost,ref rRebuy);


        t_locked.text = locked.Replace("{v}", rCost.ToString("#.0,#") + costFormat) ;
        t_active.text = active.Replace("{v}", rDps.ToString("#.0,#") + dpsFormat);
        t_active.text = t_active.text.Replace("{c}", rDur.ToString("#.0,#") + durFormat);
        t_broken.text = broken.Replace("{v}", rRebuy.ToString("#.0,#") + rebuyFormat);

        UpdateSlot();
    }

    public bool Active()
    {
        return GPU.Active;
    }
    public void LevelUpDuration(Bonus dur)
    {
        GPU.LevelUpDuration(dur);
        UpdateSlot();

    }
    public void LevelUpEfficency(Bonus eff)
    {
        GPU.LevelUpEfficency(eff);
        UpdateSlot();
    }
    public float PickMoney(bool consumeGPU = true)
    {
        float money = GPU.PickMoney(consumeGPU);
        UpdateSlot();
        return money;

    }
}
