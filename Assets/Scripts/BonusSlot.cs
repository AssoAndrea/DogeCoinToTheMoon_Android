using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class BonusSlot : MonoBehaviour
{
    public TMP_Text t_level, t_dps,t_cost,t_unlockCost;
    public GameObject Unlocked, Locked;
    public Bonus bonus;
    [HideInInspector] public float val;
    float currentCost;
    uint level;
    Shop shop;
    Resources resources;

    // Start is called before the first frame update
    void Start()
    {
        shop = GetComponentInParent<Shop>();
        resources = GameManager.instance.resources;
        t_cost.fontSize = 50;


        UpdateSlot();

        EventManager.ResourcesUpdated(resources);

    }
    private void OnEnable()
    {
        EventManager.EV_LoadData += OnLoad;
    }

    private void OnLoad(SaveData saveData)
    {
        UpdateSlot();
    }

    public void UpdateSlot(bool skipCostSet = false)
    {
        if (!skipCostSet)
        {
            currentCost = bonus.cost;
        }
        if (bonus.type == BonusT.GPUdur)
        {
            val = bonus.getGpuDurationMul();
        }
        else
        if (bonus.type == BonusT.GPUeff)
        {
            val = bonus.getGpuEffMul();
        }
        else
        {
            val = bonus.dps;
        }
        level = bonus.level;
        t_level.text = $"Level: {level}";

        float valRounded = val;
        string valFormat = "";
        if (val>1000)
        {
            Debug.Log("aooo");
            valFormat = MathHelper.FormatOverthousands(val, ref valRounded);
        }
        t_dps.text = bonus.InfoText.Replace("{v}", valRounded.ToString("#.#")+ valFormat);


        float costFormatted = currentCost;
        string costFormatting = MathHelper.FormatOverthousands(currentCost, ref costFormatted);
        string str_cost = costFormatted.ToString("0.00");
        t_cost.text = str_cost + costFormatting;

        float unlockCostFormatted = bonus.baseCost;
        string unlockFormatting = MathHelper.FormatOverthousands(bonus.baseCost, ref unlockCostFormatted);
        t_unlockCost.text = t_unlockCost.text.Replace("{v}", unlockCostFormatted.ToString("0.00")+unlockFormatting);


        Unlocked.SetActive(bonus.isUnlocked);
        Locked.SetActive(!bonus.isUnlocked);


    }

    void Unlock()
    {
        if (resources.Money > bonus.baseCost)
        {
            bonus.isUnlocked = true;
            Unlocked.SetActive(true);
            Locked.SetActive(false);
            resources.AddOrRemoveMoney(-bonus.baseCost);
            currentCost = bonus.cost;
        }
    }
    public void OnUpgrade()
    {
        if (!bonus.isUnlocked)
        {
            Unlock();
            return;
        }
        if(resources.Money >= currentCost)
        {
            if (bonus.type == BonusT.GPUdur)
            {
                shop.Gpu_farm.LevelUpDuration(val);
            }else if(bonus.type == BonusT.GPUeff)
            {
                shop.Gpu_farm.LevelUpEfficency(val);
            }
            bonus.LevelUp();
            UpdateSlot(true);

            resources.AddOrRemoveMoney(-currentCost);// da fare tra gli ultimi per far aggiornare tutto con gli eventi
            currentCost = bonus.cost;
            float costFormatted = currentCost;
            string costFormatting = MathHelper.FormatOverthousands(currentCost, ref costFormatted);
            string str_cost = costFormatted.ToString("0.00") + costFormatting;
            t_cost.text = $"{str_cost}";
        }
    }
}
