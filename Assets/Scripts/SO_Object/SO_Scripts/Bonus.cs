using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BonusT { GPUeff,GPUdur,Other}

[CreateAssetMenu(fileName = "Bonus",menuName ="SO/Bonus")]
public class Bonus : ScriptableObject
{
    [HideInInspector] public int ID = 0;

    public float baseVal;
    public float baseGpuVal;

    public float baseCost;
    public bool isUnlocked = false;
    public bool UnlockedByDefault;
    public bool isAutomaticDps = false;
    public BonusT type;

    [Multiline(3)]
    public string InfoText;


    public float costExpFactor = 200; //pi� � grande meno costa il bonus a ogni livello
    public float valExpFactor = 150; // pi� � grande meno guadagna a ogni livello 150 � buono

    public uint level = 1;

    public int dps
    {
        get
        {
            if (level ==1)
            {
                return 1;
            }else return (int)(baseVal + level + (Mathf.Pow(level, 2) / valExpFactor));
        }

        private set { }
    }
    public float cost 
    { 
        get 
        { 
            if(level == 1)
            {
                return baseCost;
            }else return (int)(baseCost + level + (Mathf.Pow(level, 2) / costExpFactor));
        }
        private set { }
    }
    

    public float getGpuEffMul()
    {
        if (level == 1)
        {
            return baseGpuVal;
        }
        return baseGpuVal + (level / valExpFactor);
    }
    public float getGpuDurationMul()
    {
        if (level == 1)
        {
            return baseGpuVal;
        }
        return baseGpuVal + (level / valExpFactor);
    }

    private void OnEnable()
    {
        ID = GetInstanceID();
    }
    public void LevelUp()
    {
        level++;
    }

    public void Reset()
    {
        level = 1;
        if (UnlockedByDefault) return;
        isUnlocked = false;
        
    }
    public void Load(Bonus_Info save)
    {
        level = save.level;
        isUnlocked = save.isUnlocked;
    }



}
