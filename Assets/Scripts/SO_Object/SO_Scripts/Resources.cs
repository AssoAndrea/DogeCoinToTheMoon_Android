using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Resources", menuName = "SO/Resources")]
public class Resources : ScriptableObject
{
    public float Money;
    public int GameLevel = 1;

    public bool reset = true;
    public float moneyMultiplier = 1;
    public Bonus[] BonusToApplyEverySecond;

    int ShopDps;
    int FarmDps;

    private void OnEnable()
    {
        EventManager.EV_ResetAll += ResetRes;
        EventManager.EV_LoadData += LoadSave;
        EventManager.EV_SaveData += Save;
    }

    private void ResetRes(bool levelUp)
    {

        Money = 0;
        moneyMultiplier = 1;
        if (levelUp)
        {
            LevelUpGame();
        }
        else
        {
            GameLevel = 0;
        }
    }
    public void LevelUpGame()
    {
        GameLevel++;
    }
    public void UpdateMultiplier(MultiplierCost levels)
    {
        moneyMultiplier = levels.GetNewMul(GameLevel);
    }

    private void Save(ref SaveData saveData)
    {
        saveData.Money = Money;
        saveData.MoneyMultiplier = moneyMultiplier;
        saveData.gameLevel = GameLevel;

        foreach (Bonus item in BonusToApplyEverySecond)
        {
            Bonus_Info bonus = new Bonus_Info
            {
                ID = item.ID,
                level = item.level,
                isUnlocked = item.isUnlocked
            };
            saveData.Bonus_data.Add(bonus);
        }
    }

    private void LoadSave(SaveData saveData)
    {
        Money = saveData.Money;
        moneyMultiplier = saveData.MoneyMultiplier;
        GameLevel = saveData.gameLevel;
        foreach (Bonus item in BonusToApplyEverySecond)
        {
            foreach (Bonus_Info info in saveData.Bonus_data)
            {
                if (item.ID == info.ID)
                {
                    item.Load(info);
                }
            }
        }
    }

    public void PickMoneyFromBonus()
    {
        foreach (Bonus bonus in BonusToApplyEverySecond)
        {
            if (bonus.isUnlocked && bonus.isAutomaticDps)
            {
                Money += bonus.dps * moneyMultiplier;

            }
        }
        EventManager.EV_ResourcesUpdate?.Invoke(this);
    }



    public void AddOrRemoveMoney(float val)
    {
        Money += val;
        EventManager.EV_ResourcesUpdate?.Invoke(this);
    }
    public void SetShopDps(int val)
    {
        ShopDps = val;
    }
    public void SetFarmDps(int val)
    {
        FarmDps = val;
    }
    public int GetDps()
    {
        return ShopDps + FarmDps;
    }
}


