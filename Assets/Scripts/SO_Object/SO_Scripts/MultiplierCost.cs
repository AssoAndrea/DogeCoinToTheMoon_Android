using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MultiplierCost", menuName = "SO/MultiplierCost")]
public class MultiplierCost : ScriptableObject
{
    public Level[] levels;

    public float GetCost(int level)
    {

        if (level > levels.Length)
        {
            return MathHelper.trilione;
        }
        return levels[level].cost;
    }
    public float GetNewMul(int level)
    {
        if (level ==0)
        {
            return 1;
        }
        if (level > levels.Length)
        {
            return MathHelper.trilione;
        }
        return levels[level].newMul;
    }
}

[Serializable]
public struct Level
{
    public float cost;
    public float newMul;
}
