using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GPU", menuName = "SO/GPU")]
public class GPU_Bonus : ScriptableObject
{
    [HideInInspector] public int ID;

    //public GPU_Slot slot;
    public bool reset;

    [Space(20)]
    public bool Active;
    public bool Locked = true;
    public float UnlockCost;
    public float rebuyCost;

    [Space(50)]
    public float dps;
    public int BaseDuration;
    public float BaseEfficency;
    public float CurrDuration;
    public float DurationToRemove = 0.5f;
    public float DurationMul;
    public float efficencyMul;

    //[Header("Level up modificator")]
    //public float effLevelMul;
    //public float durLevelMul;
    //public float effAddOnLevelUp;
    //public float DurAddOnLevelUp;
    //public float rebuyCostMul;
    //public float rebuyCostToAddOnLevelUp;


    private void OnEnable()
    {
        CurrDuration = BaseDuration * DurationMul;
        ID = GetInstanceID();
        if (reset)
        {
            Locked = true;
            Active = false;

        }
    }
    public void Load(GPU_Info save)
    {
        Active = save.active;
        Locked = save.locked;
        this.efficencyMul = save.efficency;
        CurrDuration = save.duration;
        
    }
    public float PickMoney(bool consumeGPU = true)
    {
        float val = 0;

        val = dps * efficencyMul;
        if (consumeGPU)
        {
            CurrDuration -= DurationToRemove;
        }
        if (CurrDuration <= 0)
        {
            Active = false;
        }
        return val;
    }
    public float GetCurrentDPS()
    {
        return dps * efficencyMul;
    }

    public void LevelUpDuration(Bonus dur)
    {
        DurationMul = dur.getGpuDurationMul();
        CurrDuration = BaseDuration * DurationMul;

    }
    public void LevelUpEfficency(Bonus eff)
    {
        efficencyMul = eff.getGpuEffMul();
    }

    public void Unlock()
    {
        Locked = false;
        Active = true;
        CurrDuration = BaseDuration * DurationMul;
    }
    public void Buy()
    {
        Active = true;
        //CurrDuration = BaseDuration * DurationMul;
    }
    public void Reset()
    {
        Locked = true;
        Active = false;
    }
}

