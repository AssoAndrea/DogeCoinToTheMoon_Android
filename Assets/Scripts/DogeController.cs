using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogeController : MonoBehaviour
{
    public Animator anim;
    public Bonus PickAxeBonus;
    public HUD_Spawner Points_Spawner;

    Resources resources;
    // Start is called before the first frame update
    void Start()
    {
        resources = GameManager.instance.resources;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Tap()
    {
        anim.SetTrigger("Pick");
        float MoneyPicked = PickAxeBonus.dps * resources.moneyMultiplier;
        Points_Spawner.Spawn(MoneyPicked);
        GameManager.instance.resources.AddOrRemoveMoney(MoneyPicked);
    }
}
