using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Globalization;

public class Header : MonoBehaviour
{
    public TMP_Text Money;
    public TMP_Text DPS;


    private void OnEnable()
    {
        EventManager.EV_ResourcesUpdate += OnResourceUpdated;
    }

    private void OnResourceUpdated(Resources res)
    {
        if (res.Money >= MathHelper.milione)
        {
            float fMoney = res.Money;

            string format = MathHelper.FormatOverMillion(res.Money, ref fMoney);
            Money.text = fMoney.ToString("0.000") + " " + format;
        }
        else
        {
            Money.text = res.Money.ToString("#,#");
        }
        float roundedDPS = res.GetDps();
        string formatDPS = MathHelper.FormatOverMillion(roundedDPS, ref roundedDPS);
        DPS.text = $"dps: {roundedDPS.ToString("#.#")}{formatDPS}";
        //{ MathHelper.FormatOverMillion(res.Money)}
    }
}
