using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool haveToLoad = true;
    public Resources resources;
    public string saveName = "save";
    public static uint lastID;
    [HideInInspector] public bool SaveFound;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);

    }
    // Start is called before the first frame update
    void Start()
    {
        if (!haveToLoad)
        {
            return;
        }
        if (!EventManager.LoadData())
        {
            EventManager.ResetGame();
        }
        EventManager.ResourcesUpdated(resources);
    }
    public void AddMoneyDebug()
    {
        instance.resources.AddOrRemoveMoney(MathHelper.trilione);
    }
    public static float GetADS_Bonus(int secondToSimulate,float MoneyMultiplier)
    {
        return (instance.resources.GetDps() * secondToSimulate) + instance.resources.Money * MoneyMultiplier;
    }
    public static uint GetNextID()
    {
        lastID++;
        return lastID;
    }

    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void Save()
    {
        EventManager.SaveData();
    }
    public void ResetGame()
    {
        EventManager.ResetGame();
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            EventManager.SaveData();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            EventManager.ResetGame();
        }
    }
}
