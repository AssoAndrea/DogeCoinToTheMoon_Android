using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class bonusUpdater : MonoBehaviour
{
    TMP_Text text;
    string originalString;

    public TMP_Text Text => text;

    private void OnEnable()
    {
        text = GetComponent<TMP_Text>();
        originalString = text.text;
        EventManager.EV_ResourcesUpdate += UpdateText;
    }

    private void UpdateText(Resources res)
    {
        if (GameManager.instance.resources.moneyMultiplier <= 1)
        {
            text.text = "";
        }
        else
        {
            text.text = originalString.Replace("{v}", GameManager.instance.resources.moneyMultiplier.ToString());
        }
    }
}
